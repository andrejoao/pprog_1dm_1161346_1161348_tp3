/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetofinal.model;

import java.io.Serializable;

/**
 *
 * @author andre
 */
public class Organizador implements Serializable{
    
    /**
     * email do organizador
     */
    private String idOrganizador;
    
    /**
     * construtor vazio de organizador
     */
    public Organizador()
    {
        
    }
    /**
     * construtor colpeto de organizador
     * @param idOrganizador email do organizador
     */
    public Organizador(String idOrganizador)
    {
        this.idOrganizador = idOrganizador;
    }

    /**
     * método que obtem email do organizador
     * @return email do organizador
     */
    public String getIdOrganizador() {
        return idOrganizador;
    }

    /**
     * metodo descritivo do objeto organizador
     * @return idOrganizador
     */
    @Override
    public String toString() {
        return "Organizador{" + "idOrganizador=" + idOrganizador + '}';
    }
    
}
