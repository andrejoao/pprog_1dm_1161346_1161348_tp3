/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetofinal.UI;

import projetofinal.model.Atribuicao;
import projetofinal.model.Algoritmo;
import projetofinal.model.CentroEventos;
import projetofinal.model.Evento;
import projetofinal.algoritmos.Alg1;
import projetofinal.model.Organizador;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author yngwie
 */
public class DialogoAtribuirCandidatura extends JDialog
{

   private JanelaMain framePai;
   private JPanel p1, p3;
   private Evento evento;
   private JComboBox boxEventos = new JComboBox();
   private JComboBox boxAlgoritmos = new JComboBox();
   private static Organizador organizadorLogado;

   private static final Dimension LABEL_TAMANHO
           = new JLabel("Selecionar Evento:").getPreferredSize();

   /**
    * Construtor de JDialog
    * @param framePai janela principal
    */
   public DialogoAtribuirCandidatura(JanelaMain framePai)
   {

      super(framePai, "Atribuir Candidaturas", true);

      this.framePai = framePai;
      prepararAlgoritmo();
      criarComponentes();

      setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      pack();
      setResizable(false);
      setLocationRelativeTo(framePai);
      setVisible(true);
   }

   /**
    * método que adiciona paineis à JDialog
    */
   private void criarComponentes()
   {
      p1 = criarPainelEvento();
      p3 = criarPainelBotoes();

      add(p1, BorderLayout.NORTH);
      add(p3, BorderLayout.SOUTH);
   }

   /**
    * método que cria o painel onde se seleciona o evento
    * @return JPanel
    */
   private JPanel criarPainelEvento()
   {
      JLabel lbl = new JLabel("Selecionar Evento:", JLabel.RIGHT);
      lbl.setPreferredSize(LABEL_TAMANHO);

      for (Evento e : CentroEventos.getListaEventos())
      {
         if (e.getListaOrganizadores().contains(organizadorLogado))
         {
            boxEventos.addItem(e);
         }

      }

      JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
      final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
      final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
      p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
              MARGEM_INFERIOR, MARGEM_DIREITA));

      p.add(lbl);
      p.add(boxEventos);

      return p;
   }

   /**
    * Percorre a lista de algoritmos guardados em memoria e preenche a combo box
    * repestivamente
    */
   private void prepararAlgoritmo()
   {
      for (Algoritmo alg : CentroEventos.getAlgoritmosAtribuicao())
      {
         boxAlgoritmos.addItem(alg);
      }
   }

   /**
    * método que criar painel dos botoes
    * @return JPanel
    */
   private JPanel criarPainelBotoes()
   {
      JButton btnOK = criarBotaoOK();
      getRootPane().setDefaultButton(btnOK);

      JButton btnCancelar = criarBotaoCancelar();

      JPanel p = new JPanel();
      final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
      final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
      p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
              MARGEM_INFERIOR, MARGEM_DIREITA));
      p.add(btnOK);
      p.add(btnCancelar);

      return p;
   }

   /**
    * m+etodo que criar painel de mostrar as atribuiçoes
    * @return JPanel
    */
   private JPanel criarPainelMostrarAtribuicoes()
   {
      final int espacamento = 20;
      JPanel painelDecisao = new JPanel();
      painelDecisao.setLayout(new BoxLayout(painelDecisao, BoxLayout.Y_AXIS));

      for (Atribuicao atribuicao : evento.getListaAtribuicoes())
      {
         painelDecisao.add(new JLabel(atribuicao.toString()));
      }
      painelDecisao.add(Box.createVerticalStrut(espacamento));
      painelDecisao.add(new JLabel("Confirma resultados?"));
      return painelDecisao;
   }

/**
 * método que cria o botao ok
 * @return 
 */
   private JButton criarBotaoOK()
   {
      JButton btn = new JButton("OK");
      btn.addActionListener(new ActionListener()
      {
          // Açao quando o botao ok é clicado
         @Override
         public void actionPerformed(ActionEvent e)
         {
            // Boolean para saber quando sair do ciclo na parte que o organizador esta a escolher o algortimo
            boolean ativo = false;
            
            // recebe o evento selecionado na comboBox de evento
            evento = (Evento) boxEventos.getSelectedItem();

            // Se a lista de eventos tiver vazia faz...
            if (evento.getListaAtribuicoes().isEmpty())
            {
               // Passa a lista de algoritmos para vetor de objetos a ser usado numa JOptionPane a mostrar ao organizador
               Object[] algObj = new Object[CentroEventos.getAlgoritmosAtribuicao().size()];
               for (int i = 0; i < CentroEventos.getAlgoritmosAtribuicao().size(); i++)
               {
                  algObj[i] = CentroEventos.getAlgoritmosAtribuicao().get(i);
               }
               // Ciclo de escolher o algortimo pretendio, enquanto nao confirmar algortimo
               while (!ativo)
               {
                  if (!evento.getListaCandidaturas().isEmpty() && !evento.getListaFAE().isEmpty())
                  {
                     // Janela de escolher algoritmo
                     int opcaoEscolhaAlgoritmo = JOptionPane.showOptionDialog(framePai, "Evento: " + evento + "\nEscolha algoritmo de atribuiçao", "Atribuir Candidaturas", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, algObj, null);
                     evento.getListaAtribuicoes().clear();
                     if (opcaoEscolhaAlgoritmo == 0)
                     {
                        //Janela de escolher numero max por candidatura
                        String input = JOptionPane.showInputDialog(framePai, "Numero maximo por candidatura: ", "Atribuir Candidatura", JOptionPane.QUESTION_MESSAGE);

                        // verificaçoes do input do organizador (numeros, em branco, etc)
                        try
                        {
                           int numMaxFaeCandidatura = Integer.parseInt(input);

                           if (numMaxFaeCandidatura <= evento.getListaFAE().size())
                           {
                              Alg1.setNumMaxFaePorCandidatura(numMaxFaeCandidatura);

                           } else
                           {
                              JOptionPane.showMessageDialog(framePai,"Introduziu mais faes dos que os que existem em sistema.");
                              break;
                           }
                        } catch (IllegalArgumentException exception)
                        {
                           JOptionPane.showMessageDialog(framePai,"Apenas pode introduzir numeros!");
                           break;
                        }

                     }
                     if (opcaoEscolhaAlgoritmo != JOptionPane.CLOSED_OPTION)
                     {
                        atribuirAlgoritmo(opcaoEscolhaAlgoritmo);

                     } else
                     {
                        break;
                     }
                     //Janela que mostra as atribuiçoes
                     int opcaoSimOuNao = JOptionPane.showConfirmDialog(framePai, criarPainelMostrarAtribuicoes(), "Atribuicao Candidaturas", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                     if (opcaoSimOuNao == JOptionPane.YES_OPTION && !evento.getListaAtribuicoes().isEmpty())
                     {
                        JOptionPane.showMessageDialog(framePai, "Atribuiçoes Criadas com sucesso");
                        ativo = true;
                     }
                  } else
                  {
                     JOptionPane.showMessageDialog(framePai, "Ainda nao há candidaturas/FAE");
                     ativo = true;
                  }
               }
            } else
            {
               JOptionPane.showMessageDialog(framePai, "O Evento: " + evento + " já tem as candidaturas atribuidas");
            }

         }
      });
      return btn;
   }

   /**
    * Este método preenche a lista de atribuiçoes em evento
    *
    * @param pos posicao do algoritmo na lista de algoritmos
    *
    */
   private void atribuirAlgoritmo(int pos)
   {
      evento.setListaAtribuicoes(CentroEventos.getAlgoritmosAtribuicao().get(pos).atribui(evento.getListaCandidaturas(), evento.getListaFAE()));
   }

   private JButton criarBotaoCancelar()
   {
      JButton btn = new JButton("Cancelar");
      btn.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            dispose();
         }
      });
      return btn;
   }

   public static void setOrganizadorLogado(Organizador organizador)
   {
      organizadorLogado = organizador;
   }
}
