/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetofinal.model;

import java.io.Serializable;

/**
 *
 * @author root
 */
public class Utilizador implements Serializable{
    /**
     * nome do utilizador
     */
    private String nome;
    /**
     * email do utilizador
     */
    private String email;
    /**
     * username do utilizador
     */
    private String username;
    /**
     * password do utilizador
     */
    private String password;
    
    /**
     * Construtor vazio do utilizador
     */
    public Utilizador()
    {
        
    }
    /**
     * Construtor completo do utilizador
     * @param nome do utilizador
     * @param email do utilizador
     * @param username do utilizador
     * @param password  do utilizador
     */
    public Utilizador(String nome, String email, String username,String password)
    {
        this.nome = nome;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    /**
     * método que retorna a informaçao de utilizador numa string
     * @return String
     */
    @Override
    public String toString() {
        return "Utilizador{" + "nome=" + nome + ", email=" + email + "username=" + username + "password=" + password + '}';
    }
    /**
     * método que permite obter email de utilizador
     * @return email do utilizador
     */
    public String getEmail()
    {
        return email;
    }
}
