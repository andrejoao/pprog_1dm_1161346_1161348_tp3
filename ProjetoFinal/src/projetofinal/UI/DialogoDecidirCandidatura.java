package projetofinal.UI;

import projetofinal.model.Atribuicao;
import projetofinal.model.CentroEventos;
import projetofinal.model.Decisao;
import projetofinal.model.FAE;
import projetofinal.model.Evento;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class DialogoDecidirCandidatura extends JDialog
{

   private JComboBox boxEventos;
   private JComboBox boxAtribuicoes;
   private JanelaMain framePai;
   private JPanel p1;
   private JPanel p2;
   private static FAE faeLogado;

   public DialogoDecidirCandidatura(JanelaMain framePai)
   {

      super(framePai, "Decidir Candidatura", true);

      this.framePai = framePai;

      criarComponentes();

      setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      pack();
      setResizable(false);
      setLocationRelativeTo(framePai);
      setVisible(true);
   }

   private void criarComponentes()
   {

      JPanel pCentro = new JPanel();

      p1 = criarPainelEventos();
      p2 = criarPainelBotoes();

      pCentro.setLayout(new BorderLayout());
      pCentro.add(p1, BorderLayout.NORTH);

      add(p1, BorderLayout.NORTH);
      add(p2, BorderLayout.SOUTH);
   }

   private JPanel criarPainelEventos()
   {
      String emailFAE;
      JLabel lbl = new JLabel("Selecione o evento para decidir as candidaturas:", JLabel.LEFT);
      lbl.setPreferredSize(lbl.getPreferredSize());

      boxEventos = new JComboBox();
      for (Evento e : CentroEventos.getListaEventos())
      {
         if(e.getListaFAE().contains(faeLogado))
         {
            boxEventos.addItem(e);
         } 
      }

      JPanel p = new JPanel();
      final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
      final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
      p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
              MARGEM_INFERIOR, MARGEM_DIREITA));
      p.add(lbl);
      p.add(Box.createHorizontalStrut(15));
      p.add(boxEventos);

      return p;
   }
   
   private JPanel criarPainelBotoes()
   {
      JButton btnOK = criarBotaoOK();
      getRootPane().setDefaultButton(btnOK);

      JButton btnCancelar = criarBotaoCancelar();

      JPanel p = new JPanel();
      final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
      final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
      p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
              MARGEM_INFERIOR, MARGEM_DIREITA));
      p.add(btnOK);
      p.add(btnCancelar);

      return p;
   }

   private JButton criarBotaoOK()
   {
      JButton btn = new JButton("OK");
      btn.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {

            final int espacamento = 20;

            Evento eventoSelecionado = (Evento) boxEventos.getSelectedItem();

            //dialogo escolher atribuicao
            boxAtribuicoes = new JComboBox();
            for (Atribuicao a : eventoSelecionado.getAtribuicoesFae(faeLogado))
            {
               boxAtribuicoes.addItem(a);
            }

            if (boxAtribuicoes.getItemCount() != 0)
            {
               JPanel painel = new JPanel();
               painel.add(new JLabel("Seleciona uma das candidaturas:"));
               painel.add(Box.createHorizontalStrut(espacamento));
               painel.add(boxAtribuicoes);

               int opcaoDecidirCandidatura = JOptionPane.showConfirmDialog(btn, painel, "Decidir Candidatura", JOptionPane.OK_CANCEL_OPTION);

               if (opcaoDecidirCandidatura != JOptionPane.CANCEL_OPTION && opcaoDecidirCandidatura != JOptionPane.CLOSED_OPTION)
               {
                  Atribuicao atribuicaoADecidir = (Atribuicao) boxAtribuicoes.getSelectedItem();

                  //dialogo inserir decisao e justificacao                    
                  JPanel painelDecisao = new JPanel();
                  painelDecisao.setLayout(new BoxLayout(painelDecisao, BoxLayout.Y_AXIS));

                  painelDecisao.add(new JLabel("Detalhe da candidatura:"));
                  painelDecisao.add(new JLabel(atribuicaoADecidir.getCandidatura().getTextoDescrivo()));
                  painelDecisao.add(Box.createVerticalStrut(espacamento));
                  painelDecisao.add(new JLabel("Insira decisão: "));
                  JRadioButton botaoAprovado = new JRadioButton("Aprovado ", false);
                  painelDecisao.add(botaoAprovado);
                  painelDecisao.add(Box.createVerticalStrut(espacamento));
                  painelDecisao.add(new JLabel("Insira Justificação: "));

                  String justificacao = (String) JOptionPane.showInputDialog(btn, painelDecisao, "Decidir Candidatura", JOptionPane.QUESTION_MESSAGE, null, null, null);

                  if (justificacao != null)
                  {

                     while (justificacao.isEmpty())
                     {
                        JOptionPane.showMessageDialog(btn, "Tem que inserir uma descrição.");
                        justificacao = (String) JOptionPane.showInputDialog(btn, painelDecisao, "Decidir Candidatura", JOptionPane.QUESTION_MESSAGE, null, null, null);

                     }

                     boolean decisao = botaoAprovado.isSelected();
                     Decisao d = new Decisao(decisao, justificacao);
                     atribuicaoADecidir.setDecisao(d);
                     JOptionPane.showMessageDialog(btn, "Candidatura decidida com sucesso!");
                  }
               }

            }
            else
            {
               JOptionPane.showMessageDialog(btn, "Não existem candidaturas a decidir para o seu utilizador.");
            }
         }
      });
      return btn;
   }

   private JButton criarBotaoCancelar()
   {
      JButton btn = new JButton("Cancelar");
      btn.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            dispose();
         }
      });
      return btn;
   }

   public static void setFAELogado(FAE fae)
   {
      faeLogado = fae;
   }
}
