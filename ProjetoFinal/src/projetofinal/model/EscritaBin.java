package projetofinal.model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Classe para escrever em ficheiro binario
 *
 * @author Joao
 */
public class EscritaBin
{

   /**
    * construtor vazio do objeto escrita
    */
   public EscritaBin()
   {

   }
   /**
    * contante com o nome do ficheiro a criar
    */
   private final String NOME_FICHEIRO = "ListaEventos.bin";

   /**
    * Metodo de instancia para guardar uma lista de eventos num determinado
    * ficheiro binario
    *
    * @param listaEventos lista de eventos a guardar
    * @return true se guardar com sucesso ou false se ocorrer alguma excecao
    */
   public boolean guardar(ArrayList<Evento> listaEventos)
   {
      try
      {
         ObjectOutputStream out = new ObjectOutputStream(
                 new FileOutputStream(NOME_FICHEIRO));
         try
         {
            out.writeObject(listaEventos);
         } finally
         {
            out.close();
         }
         return true;

      } catch (IOException ex)
      {
         return false;
      }

   }
}
