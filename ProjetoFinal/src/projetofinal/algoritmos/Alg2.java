package projetofinal.algoritmos;

import java.util.ArrayList;
import projetofinal.model.Algoritmo;
import projetofinal.model.Atribuicao;
import projetofinal.model.Candidatura;
import projetofinal.model.FAE;

/**
 * Algoritmo de distribuição de carga equitativa pelos FAE
 *
 * @author Joao
 */
public class Alg2 implements Algoritmo
{

   private final String NOME = "Algoritmo 2";
   /**
    * lista de atribuiçoes a retornar
    */
   ArrayList<Atribuicao> listaAtribuicao = new ArrayList<>();

   /**
    *
    * @param listaCandidatura
    * @param listaFaes
    * @return
    */
   @Override
   public ArrayList<Atribuicao> atribui(ArrayList<Candidatura> listaCandidatura, ArrayList<FAE> listaFaes)
   {
      //se o numero de candidaturas for maior que o numero de faes
      //contamos quantas vezes o numero de candidatuas cabe no numero de faes
      //com a variavel div. o metodo arredondar serve para não ficar nenhuma
      //candidatura por atribuir.

      if (listaCandidatura.size() > listaFaes.size())
      {
         int div = (int) arredondar((double) listaCandidatura.size() / listaFaes.size());
         int k = 0;
         for (int i = 0; i < div; i++)
         {
            for (FAE fae : listaFaes)
            {
               // este try é necessario pois quando chegarmos ao numero total de 
               // candidaturas não é possivel pararmos o ciclo a meio, sendo 
               // que as atribuiçoes são feitas corretamente, a única necessidade
               // que temos é de apanhar a exceçao de forma a não interromper o 
               // programa
               try
               {
                  listaAtribuicao.add(new Atribuicao(fae, listaCandidatura.get(k)));
                  k++;
               } catch (IndexOutOfBoundsException e)
               {
                  break;
               }

            }

         }

         return listaAtribuicao;
      } else
      {
         for (int i = 0; i < listaCandidatura.size(); i++)
         {

            listaAtribuicao.add(new Atribuicao(listaFaes.get(i), listaCandidatura.get(i)));
         }
      }

      return listaAtribuicao;
   }

   /**
    * Metodo para arredondar sempre um numero por excesso, caso ele tenha casas
    * decimais
    *
    * @param num numero a arredondar
    * @return numero arredondado
    */
   public int arredondar(double num)
   {
      if ((num - (int) num) > 0.0)
      {
         num += 1;
      }
      return (int) num;
   }

   /**
    * Reescrita do metodo toString para apresentar o nome do algoritmo
    *
    * @return nome do algoritmo
    */
   @Override
   public String toString()
   {
      return NOME;
   }

}
