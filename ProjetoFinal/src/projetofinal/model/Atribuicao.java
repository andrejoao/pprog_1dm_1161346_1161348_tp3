package projetofinal.model;

import java.io.Serializable;

/**
 * Classe atribuição
 * @author Joao
 */
public class Atribuicao implements Serializable{

   /**
    * Fae a atribuir a uma candidatura
    */
    private FAE fae;
    /**
     * Candidatura a atribuir a um Fae
     */
    private Candidatura candidatura;
    /**
     * Decisao que fae irá decidir sobre candidatura atribuida
     */
    private Decisao decisao;
    
    /**
     * Construtor completo 
     * @param fae fae pretendido para atribuir
     * @param candidatura candidatura pretendida para atribuir
     */
    public Atribuicao(FAE fae, Candidatura candidatura) {
        this.fae = fae;
        this.candidatura = candidatura;
        decisao = new Decisao();
    }

    /**
     * metodo para obter fae da atribuicao
     * @return a referencia para o objeto fae de uma atribuição
     */
   public FAE getFae()
   {
      return fae;
   }

   /**
    * metodo para obter candidatura da atribuicao
    * @return candidatura
    */
   public Candidatura getCandidatura()
   {
      return candidatura;
   }

   /**
    * metodo para obter decisao da atribuicao
    * @return decisao
    */
   public Decisao getDecisao()
   {
      return decisao;
   }

   /**
    * metodo para alterar decisao de atribuicao
    * @param decisao 
    */
   public void setDecisao(Decisao decisao)
   {
      this.decisao = decisao;
   }

   /**
    * metodo toString especifico para este objeto
    * @return string com o texto descritivo da candidatura de atribuiçao e do fae da atribuiçao
    */
    @Override
    public String toString() {
        return "Foi atribuida a candidatura: " + candidatura.getTextoDescrivo() + " ao FAE: " + fae.getID();
    }
   
   
   


    
}
