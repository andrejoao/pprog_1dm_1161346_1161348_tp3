package projetofinal.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Classe candidatura
 * @author Joao
 */
public class Candidatura implements Serializable
{

   /**
    * texto que descreve uma candidatura
    */
   private String textoDescritivo;
   /**
    * partipante(representante) que cria candidatura
    */

   private Utilizador participante;

   /**
    * lista de atribuicoes que vai guardar todas as atribuicoes da candidatura
    */
   private ArrayList<Atribuicao> listaAtribuicoes = new ArrayList<>();

   /**
    * Construtor vazio pois apenas interessa dar valor às variaveis de instancia
    * após criação do objeto
    */
   public Candidatura()
   {

   }

   /**
    * Metodo para obter descrico da candidatura
    *
    * @return descricao da candidatura
    */
   public String getTextoDescrivo()
   {
      return textoDescritivo;
   }

   /**
    * Metodo para alterar a descricao da candidatura
    *
    * @param textoDescrivo texto alterado
    */
   public void setTextoDescrivo(String textoDescrivo)
   {
      this.textoDescritivo = textoDescrivo;
   }

   /**
    * Metodo para obter lista de atribuicoes da candidatura
    *
    * @return lista de atribuicoes
    */
   public ArrayList<Atribuicao> getListaAtribuicoes()
   {
      return listaAtribuicoes;
   }

   /**
    * Metodo para inserir participante que cria candidatura
    *
    * @param participante utilizador logado no momento de criaçao
    */
   public void setParticipante(Utilizador participante)
   {
      this.participante = participante;
   }

   /**
    * Metodo para obter participante(representante) que criou candidatura
    *
    * @return participante(representante)
    */
   public Utilizador getParticipante()
   {
      return participante;
   }

   /**
    * Metodo para adicionar uma nova atribuição à lista de atribuiçoes da
    * candidatura
    *
    * @param atribuicao atribuiçao a ser adicionada
    */
   public void addAtribuicao(Atribuicao atribuicao)
   {
      listaAtribuicoes.add(atribuicao);
   }

   /**
    * Reescrita do metodo toString onde é apresentado o texto descritivo da
    * candidatura
    *
    * @return String textodescritivo
    */
   @Override
   public String toString()
   {
      return "Candidatura:" + textoDescritivo;
   }

}
