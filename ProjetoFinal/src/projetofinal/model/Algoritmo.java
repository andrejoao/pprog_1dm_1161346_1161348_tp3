package projetofinal.model;

import java.util.ArrayList;

/**
 * Interface algoritmo para implemetanção do método atribui que obriga o retorno de um arraylist do tipo atribuição. 
 * Como parametros é inserida uma lista do tipo candidaturas e uma lista do tipo FAES. 
 * Todos os algoritmos que implementarem esta interface, deverão associar um objeto do tipo candidatura e um objeto do tipo fae
 * num objeto do tipo atribuição que posteriormente será guardado numa lista de atribuições e retornado.
 * @author Joao
 */
public interface Algoritmo {
    public ArrayList<Atribuicao> atribui(ArrayList<Candidatura> listaCandidatura, ArrayList<FAE> listaFaes);
}
