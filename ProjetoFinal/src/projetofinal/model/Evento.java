package projetofinal.model;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Andre
 */
public class Evento implements Serializable
{

   /**
    * variavel que guarda a lista de candidaturas
    */
   private ArrayList<Candidatura> listaCandidaturas = new ArrayList<>();
   /**
    * titulo do evento
    */
   private String titulo;
   /**
    * descricao do evento
    */
   private String descricao;
   /**
    * data de inicio do evento
    */
   private String dataInicio;
   /**
    * data de fim do evento
    */
   private String dataFim;
   /**
    * local do evento
    */
   private String local;
   /**
    * lista de organizadores do evento
    */
   private ArrayList<Organizador> listaOrganizadores = new ArrayList<>();
   /**
    * lista de FAEs do evento
    */
   private ArrayList<FAE> listaFAE = new ArrayList<>();
   /**
    * lista de atribuiçoes do evento
    */
   private ArrayList<Atribuicao> listaAtribuicoes = new ArrayList<>();

   /**
    * Construtor vazio
    */
   public Evento()
   {

   }

   /**
    * construtor completo de vento
    * @param titulo titulo do evento
    * @param descricao descricao do evento
    * @param dataInicio data inicial do evento
    * @param dataFim data de fim do evento
    * @param local local do evento
    */
   public Evento(String titulo, String descricao, String dataInicio, String dataFim, String local)
   {
      this.titulo = titulo;
      this.descricao = descricao;
      this.dataInicio = dataInicio;
      this.dataFim = dataFim;
      this.local = local;
   }

   /**
    * metodo descritivo do evento, apenas mostra o titulo
    * @return titulo
    */
   @Override
   public String toString()
   {
      return titulo;
   }

   /**
    * método para obter lista de organizadores
    * @return listaOrganizadores
    */
   public ArrayList<Organizador> getListaOrganizadores()
   {
      return listaOrganizadores;
   }

   /**
    * método para obter lista de faes
    * @return listaFAE
    */
   public ArrayList<FAE> getListaFAE()
   {
      return listaFAE;
   }

   /**
    * método para adicionar FAE à lista de fae
    * @param fae para adicionar à lista de faes
    */
   public void addFAE(FAE fae)
   {
      listaFAE.add(fae);
   }

   /**
    * método para adicionar organizador à lista de organizadores
    * @param org para adicionar à lista de organizadores
    */
   public void addOrganizador(Organizador org)
   {
      listaOrganizadores.add(org);
   }

   /**
    * método para obter titulo do evento
    * @return titulo
    */
   public String getTitulo()
   {
      return titulo;
   }
   /**
    * método para obter lista atribuiçoes do evento
    * @return listaAtribuiçoes
    */
    public ArrayList<Atribuicao> getListaAtribuicoes() {
        return listaAtribuicoes;
    }
   

   /**
    * método para obter a lista de atribuições de um FAE passado por parametro
    * @param fae que pretendemos obter a lista de atribuiçoes
    * @return listaAtribuicoesAux
    */
   public ArrayList<Atribuicao> getAtribuicoesFae(FAE fae)
   {
      ArrayList<Atribuicao> listaAtribuicoesAux = new ArrayList<>();

      for (Atribuicao atribuicao : listaAtribuicoes)
      {
         if (atribuicao.getFae().getID().contentEquals(fae.getID()))
         {
            if (verificaSeFAEJaPreencheu(atribuicao.getDecisao()))
            {
               listaAtribuicoesAux.add(atribuicao);
            }
         }
      }
      return listaAtribuicoesAux;
   }

 
   
   /**
    * método que verifica se FAE ja preencheu decisao
    * @param d objeto decisao passado por parametro
    * @return true se nao tiver preenchido, return false se ja tiver preenchido
    */
   private boolean verificaSeFAEJaPreencheu(Decisao d)
   {
       if(d.getJustificacao().isEmpty())
       {
           return true;
       }
       return false;
   }
   
   /**
    * método para obter texto descritivo da candidatura
    * @param candidatura da qual obtemos o texto descritivo
    * @return texto descritivo da candidatura
    */
   public String getInfoCandidatura(Candidatura candidatura)
   {
      return candidatura.getTextoDescrivo();
   }

   /**
    * método para obter a lista de candidaturas
    * @return listaCandidaturas
    */
   public ArrayList<Candidatura> getListaCandidaturas()
   {
      return listaCandidaturas;
   }

   /**
    * método para instancia uma nova candidatura
    * @return candidatura
    */
   public Candidatura novaCandidatura()
   {
      Candidatura c = new Candidatura();

      return c;
   }

   /**
    * método que verifica se o participante da candidatura passada por parametro
    é igual ao participante da candidatura na lista de candidaturas
    * @param c
    * @return true se a candidatura for valida, return false se nao for valida
    */
   private boolean validaCandidatura(Candidatura c)
   {
      for (Candidatura candidatura : listaCandidaturas)
      {
         if (c.getParticipante().equals(candidatura.getParticipante()))
         {
            return false;
         }
      }
      return true;
   }

   /**
    * método que adiciona uma candidatura a uma lista de candidaturas
    * @param c candidatura passada por parametro
    * @return true se candidatura for adiconada à lista, return false se nao for
    */
   public boolean addCandidatura(Candidatura c)
   {
      if (validaCandidatura(c))
      {
         listaCandidaturas.add(c);
         return true;
      }
      return false;
   }
   
   /**
    * método que permite modifica a lista de atribuiçoes
    * @param listaAtribuicoes lista de atribuiçoes que queremos atribuir
    */
   public void setListaAtribuicoes(ArrayList<Atribuicao> listaAtribuicoes)
   {
       listaAtribuicoes = listaAtribuicoes;
   }
}
