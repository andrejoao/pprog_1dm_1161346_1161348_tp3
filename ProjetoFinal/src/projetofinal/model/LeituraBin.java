package projetofinal.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 *
 * @author Andre
 */
public class LeituraBin {

    /**
     * Nome do ficheiro de leitura
     */
    public static final String NOME_FICHEIRO_EVENTOS = "ListaEventos.bin";

    /**
     * Construtor vazio
     */
    public LeituraBin() {
    }

    /**
     * método que permite a leitura de um ficheiro bin
     * @return ArrayList listaEventos
     */
    public ArrayList lerEventos() {

        ArrayList<Evento> listaEventos = new ArrayList();

        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(NOME_FICHEIRO_EVENTOS));
            try {

                listaEventos = (ArrayList) in.readObject();
            } finally {
                in.close();
            }
            return listaEventos;
        } catch (IOException | ClassNotFoundException ex) {
           ex.printStackTrace();
            return null;
        }
    }

    

    
}
