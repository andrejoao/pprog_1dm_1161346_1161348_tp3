/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetofinal.model;

import java.util.ArrayList;
import projetofinal.UI.JanelaMain;

/**
 *
 * @author root
 */
public class Teste {

    public static void main(String[] args) throws Exception {
        // TODO code application logic here

        // Instancias de eventos
        Evento e1 = new Evento("Porto-Benfica", "desc", "dataInicio", "dataFinal", "local");
        Evento e2 = new Evento("WebSummit", "desc", "dataInicio", "dataFinal", "local");
        Evento e3 = new Evento("Circo Soleil", "desc", "dataInicio", "dataFinal", "local");
        
        // Add os eventos criados à lista de eventos em centro de eventos
        CentroEventos.getListaEventos().add(e1);
        CentroEventos.getListaEventos().add(e2);
        CentroEventos.getListaEventos().add(e3);

        instanciarObjetos(CentroEventos.getListaEventos());

        LeituraTxt.lerAlgoritmo("ficheiroAlgoritmos.txt");

        JanelaMain j = new JanelaMain();
        j.setVisible(true);

    }
    
    /**
     * método que lê todos os ficheiro do tipo "DadosEventoX.txt" de uma só vez
     * @param listaEventos lista de eventos em centro de eventos
     * @throws Exception caso nao haja ficheiros de texto
     */
    private static void instanciarObjetos(ArrayList<Evento> listaEventos) throws Exception
    {
        int k = 1;
        for(int i = 0; i < listaEventos.size(); i++)
        {
            LeituraTxt.carregarDados(listaEventos.get(i), "DadosEvento" + k + ".txt");
            k++;
        }
    }

    

}
