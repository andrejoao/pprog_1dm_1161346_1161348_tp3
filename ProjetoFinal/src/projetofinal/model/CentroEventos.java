package projetofinal.model;

import java.util.*;

/**
 * classe estatica centro eventos
 *
 * @author Joao
 */
public class CentroEventos
{

   /**
    * lista que contem todos os eventos da aplicação
    */
   private static ArrayList<Evento> listaEventos = new ArrayList<>();
   /**
    * lista que contem todos os utilizadores da aplicação
    */
   private static ArrayList<Utilizador> listaUtilizadores = new ArrayList<>();
   /**
    * lista que contem todos os algoritmos da aplicação
    */
   private static ArrayList<Algoritmo> listaAlgoritmos = new ArrayList<>();

   /**
    * Metodo para adicionar um evento à lista de eventos
    *
    * @param e evento a adicionar
    */
   public static void addEvento(Evento e)
   {
      listaEventos.add(e);
   }

   /**
    * Metodo para adicionar um utilizador à lista de eventos
    *
    * @param u utilizador a adicionar
    */
   public static void addUtilizador(Utilizador u)
   {
      listaUtilizadores.add(u);
   }

   /**
    * Metodo para obter lista de eventos
    *
    * @return listaEventos
    */
   public static ArrayList<Evento> getListaEventos()
   {
      return listaEventos;
   }

   /**
    * Metodo para obter lista de utilizadores
    *
    * @return listaUtilizadores
    */
   public static ArrayList<Utilizador> getListaUtilizadores()
   {
      return listaUtilizadores;
   }

   /**
    * Metodo para obter lista de algoritmos existentes
    *
    * @return listaAlgoritmos
    */
   public static ArrayList<Algoritmo> getAlgoritmosAtribuicao()
   {
      return listaAlgoritmos;
   }

   /**
    * Metodo para inserir lista de eventos
    *
    * @param listaEventos lista de eventos a inserir
    */
   public static void setListaEventos(ArrayList<Evento> listaEventos)
   {
      CentroEventos.listaEventos = listaEventos;
   }

}
