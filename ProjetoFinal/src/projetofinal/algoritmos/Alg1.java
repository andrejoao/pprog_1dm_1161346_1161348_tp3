package projetofinal.algoritmos;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import projetofinal.model.Algoritmo;
import projetofinal.model.Atribuicao;
import projetofinal.model.Candidatura;
import projetofinal.model.FAE;

/**
 * Algoritmo que se basea no número de FAE pretendidos por candidatura
 *
 * @author Joao
 */
public class Alg1 implements Algoritmo
{

   /**
    * variavel estatica que representa o num max de fae por candidatura a ser
    * introduzida
    */
   private static int numMaxFAEPorCandidatura;
   /**
    * nome do algoritmo
    */
   private final String NOME = "Algortimo 1";
   /**
    * lsita de atribuiçoes a serem geradas
    */
   ArrayList<Atribuicao> listaAtribuicoes = new ArrayList<>();

   /**
    *
    *
    * @param listaCandidatura
    * @param listaFaes
    * @return
    */
   @Override
   public ArrayList<Atribuicao> atribui(ArrayList<Candidatura> listaCandidatura, ArrayList<FAE> listaFaes)
   {
      // se o numero de fases for menor que o num max de faes por candidatura
      // é impossivel fazer a atribuiçao
      if (listaFaes.size() < numMaxFAEPorCandidatura)
      {
         JOptionPane.showMessageDialog(null, "Número inválido");
         return null;
      } else
      {
         for (Candidatura candidatura : listaCandidatura)
         {
            for (int i = 0; i < numMaxFAEPorCandidatura; i++)
            {

               listaAtribuicoes.add(new Atribuicao(listaFaes.get(i), candidatura));

            }
         }
         return listaAtribuicoes;
      }
   }

   /**
    * metodo para inserir num max fae por candidatura
    *
    * @param num
    */
   public static void setNumMaxFaePorCandidatura(int num)
   {
      numMaxFAEPorCandidatura = num;
   }

   /**
    * Reescrita do metodo toString para apresentar o nome do algoritmo
    *
    * @return nome do algoritmo
    */
   @Override
   public String toString()
   {
      return NOME;
   }

}
