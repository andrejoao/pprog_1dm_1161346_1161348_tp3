/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetofinal.model;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author Andre
 */
public class LeituraTxt {

    /**
     * método que faz instancia objetos a partir das linhas de um ficheiro de text
     * @param evento evento para o qual se pretende instanciar os obejtos
     * @param nomefich ficheiro que contem os dados
     * @throws Exception para o caso de nao existir nenhum ficheiro
     */
    public static void carregarDados(Evento evento, String nomefich) throws Exception {

        Scanner ler = new Scanner(new File(nomefich));
        String[] word;

        while (ler.hasNextLine()) {

            String linha = ler.nextLine();
            word = linha.split(";");
            if (!linha.equals("") && word[0].equals("utilizador")) {
                CentroEventos.addUtilizador(new Utilizador(word[1], word[2], word[3], word[4]));
            } else 
                if (!linha.equals("") && word[0].equals("fae")) {
                    for (Utilizador u : CentroEventos.getListaUtilizadores()) {
                        if (u.getEmail().equals(word[1])) {
                            evento.addFAE(new FAE(word[1], Integer.parseInt(word[2])));
                        }
                    }
            } else 
                if (!linha.equals("") && word[0].equals("organizador")) {
                    for (Utilizador u : CentroEventos.getListaUtilizadores()) {
                        if (u.getEmail().equals(word[1])) {
                            evento.addOrganizador(new Organizador(word[1]));
                        }
                    }
            }
        }
    }
    /**
     * método que lê o ficheiro dos algoritmos, caso seja adicionada um novo algoritmo ele lê e adiciona à lista de algoritmos
     * @param nomeFich ficheiro que contem os algoritmos
     * @throws Exception se nao existir o ficheiro
     */
    public static void lerAlgoritmo(String nomeFich) throws Exception
    {
        Scanner ler = new Scanner(new File(nomeFich));

        while (ler.hasNextLine()) {

            String linha = ler.nextLine().trim();
            
            Algoritmo algoritmo = (Algoritmo)Class.forName(linha).newInstance();
            CentroEventos.getAlgoritmosAtribuicao().add(algoritmo);
            
        }
    }
}
