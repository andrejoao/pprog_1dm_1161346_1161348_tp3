package projetofinal.model;

import java.io.Serializable;

/**
 * classe decisao
 *
 * @author Joao
 */
public class Decisao implements Serializable
{

   /**
    * Justificacao para o construtor vazio
    */
   private static final String JUSTIFICACAO_DEFEITO = "";
   /**
    * boolean decisao que indica se a decisao é positiva ou negativa
    */
   private boolean decisao;
   /**
    * String justificacao que guarda um texto com o motivo da decisao
    */
   private String justificacao;

   /**
    * Construtor vazio de decisao;
    */
   public Decisao()
   {
      justificacao = JUSTIFICACAO_DEFEITO;
   }

   /**
    * Construtor completo de decisao
    *
    * @param decisao decisao a guardar em decisao
    * @param justificacao justificacao a guardar em decisao
    */
   public Decisao(boolean decisao, String justificacao)
   {
      this.decisao = decisao;
      this.justificacao = justificacao;
   }

   /**
    * Metodo para obter decisao(positiva ou negativa)
    *
    * @return decisao
    */
   public boolean getDecisao()
   {
      return decisao;
   }

   /**
    * Metodo para obter justificacao da decisao
    *
    * @return justificacao
    */
   public String getJustificacao()
   {
      return justificacao;
   }

   /**
    * Metodo para alterar conteudo da decisao no objeto decisao
    *
    * @param decisao (verdadeiro ou falso)
    */
   public void setDecisao(boolean decisao)
   {
      this.decisao = decisao;
   }

   /**
    * Reescrita do metodo toString com os atributos do objeto decisao
    *
    * @return String com toda a informação do objeto
    */
   @Override
   public String toString()
   {
      return "Decisao{" + "decisao=" + decisao + ", justificacao=" + justificacao + '}';
   }

}
