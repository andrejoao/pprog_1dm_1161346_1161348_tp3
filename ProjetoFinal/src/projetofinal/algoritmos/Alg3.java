package projetofinal.algoritmos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import projetofinal.model.Algoritmo;
import projetofinal.model.Atribuicao;
import projetofinal.model.Candidatura;
import projetofinal.model.FAE;

/**
 * Algoritmo que gera atribuições tendo em conta a experiencia dos FAE, sendo
 * que dá prioridade aos FAE com maior experiencia
 *
 * @author Joao
 */
public class Alg3 implements Algoritmo
{

   private final String NOME = "Algoritmo 3";

   /**
    * implementação da interface algoritmo
    *
    * @param listaCandidatura lista de candidaturas
    * @param listaFaes lista de faes
    * @return lista de atribuicoes
    */
   @Override
   public ArrayList<Atribuicao> atribui(ArrayList<Candidatura> listaCandidatura, ArrayList<FAE> listaFaes)
   {
      // utilizamos o metodo sort para ordenar de forma crescente de experiencia
      // mas como queremos que os Faes com mais experiencia tenham prioridade
      // nas atribuioçoes, revertemos a lista para ficar de forma descrescente
      Collections.sort(listaFaes, Collections.reverseOrder());

      Alg2 a = new Alg2();
      // o restante deste algoritmo é semelhante ao algoritmo2, dai podermos 
      // utiliza-lo de forma a re-aproveitar codigo
      return a.atribui(listaCandidatura, listaFaes);
   }

   /**
    * Reescrita do metodo toString para apresentar o nome do algoritmo
    *
    * @return nome do algoritmo
    */
   @Override
   public String toString()
   {
      return NOME;
   }

}
