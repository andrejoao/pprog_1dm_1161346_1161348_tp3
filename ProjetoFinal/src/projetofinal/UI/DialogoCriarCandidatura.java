package projetofinal.UI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import projetofinal.model.Candidatura;
import projetofinal.model.CentroEventos;
import projetofinal.model.Evento;
import projetofinal.model.Utilizador;

public class DialogoCriarCandidatura extends JDialog
{

   private JTextField txtDescricao;
   private JComboBox boxEventos;
   private JanelaMain framePai;

   private JPanel p1;
   private JPanel p2;

   private static Utilizador participanteLogado;

   public DialogoCriarCandidatura(JanelaMain framePai)
   {

      super(framePai, "Criar Candidatura", true);

      this.framePai = framePai;

      criarComponentes();

      setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      pack();
      setResizable(false);
      setLocationRelativeTo(framePai);
      setVisible(true);
   }

   private void criarComponentes()
   {

      JPanel pCentro = new JPanel();

      p1 = criarPainelEventos();
      p2 = criarPainelBotoes();

      pCentro.setLayout(new BorderLayout());
      pCentro.add(p1, BorderLayout.NORTH);

      add(p1, BorderLayout.NORTH);

      add(p2, BorderLayout.SOUTH);
   }

   private JPanel criarPainelInserirDescricao()
   {
      JLabel lbl = new JLabel("Insira descrição da candidatura:", JLabel.LEFT);
      lbl.setPreferredSize(lbl.getPreferredSize());

      final int CAMPO_LARGURA = 20;
      txtDescricao = new JTextField(CAMPO_LARGURA);

      JPanel p = new JPanel(new BorderLayout());
      final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
      final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
      p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
              MARGEM_INFERIOR, MARGEM_DIREITA));

      p.add(lbl, BorderLayout.NORTH);
      p.add(txtDescricao, BorderLayout.SOUTH);

      return p;
   }

   private JPanel criarPainelEventos()
   {

      JLabel lbl = new JLabel("Selecione o evento ao qual se pretende candidatar:", JLabel.LEFT);
      lbl.setPreferredSize(lbl.getPreferredSize());

      boxEventos = new JComboBox();
      for (Evento e : CentroEventos.getListaEventos())
      {
         if(verificaCandidaturaJaInseridaPorParticipante(e, participanteLogado))
         {
            boxEventos.addItem(e);
         }   
      }

      JPanel p = new JPanel(new BorderLayout());
      final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
      final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
      p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
              MARGEM_INFERIOR, MARGEM_DIREITA));
      p.add(lbl, BorderLayout.NORTH);
      p.add(boxEventos, BorderLayout.SOUTH);

      return p;
   }

   private JPanel criarPainelBotoes()
   {
      JButton btnOK = criarBotaoOK();
      getRootPane().setDefaultButton(btnOK);

      JButton btnCancelar = criarBotaoCancelar();

      JPanel p = new JPanel();
      final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
      final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
      p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
              MARGEM_INFERIOR, MARGEM_DIREITA));
      p.add(btnOK);
      p.add(btnCancelar);

      return p;
   }

   private JButton criarBotaoOK()
   {
      JButton btn = new JButton("OK");
      btn.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {

            Evento eventoSelecionado = (Evento) boxEventos.getSelectedItem();

            Candidatura novaCandidatura = eventoSelecionado.novaCandidatura();

            String textoDescricao = JOptionPane.showInputDialog(framePai, "Insira descrição candidatura:", "Criar candidatura", JOptionPane.QUESTION_MESSAGE);

            while (textoDescricao != null && textoDescricao.contentEquals(""))
            {
               JOptionPane.showMessageDialog(null, "Não inseriu qualquer informação.");
               textoDescricao = JOptionPane.showInputDialog(framePai, "Insira descrição candidatura:", "Criar candidatura", JOptionPane.QUESTION_MESSAGE);
            }

            if (textoDescricao != null)
            {
               novaCandidatura.setTextoDescrivo(textoDescricao);
               novaCandidatura.setParticipante(participanteLogado);

               boolean candidaturaAdicionada;
               boolean participanteNaoRepetido;

               if (JOptionPane.showConfirmDialog(framePai, ("Confirma candidatura?\n") + novaCandidatura.getTextoDescrivo(), "Confirma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
               {

                  //participanteNaoRepetido = verificaCandidaturaJaInseridaPorParticipante(eventoSelecionado, participanteLogado);
                  participanteNaoRepetido = eventoSelecionado.addCandidatura(novaCandidatura);
                  dispose();

                  if (participanteNaoRepetido)
                  {

                        JOptionPane.showMessageDialog(framePai, "Candidatura inserida com sucesso.");

                  } else
                  {
                     JOptionPane.showMessageDialog(framePai, "Participante já se candidatou.");
                  }

               }
            }

         }
      });
      return btn;
   }

   private boolean verificaCandidaturaJaInseridaPorParticipante(Evento e, Utilizador u)
   {
      for (Candidatura c : e.getListaCandidaturas())
      {
         if (c.getParticipante().equals(u))
         {
            return false;
         }
      }
      return true;
   }

   private JButton criarBotaoCancelar()
   {
      JButton btn = new JButton("Cancelar");
      btn.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            dispose();
         }
      });
      return btn;
   }

   public static void setParticipanteLogado(Utilizador u)
   {
      participanteLogado = u;
   }
}
