/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetofinal.model;

import java.io.Serializable;

/**
 *
 * @author Andre
 */
public class FAE implements Comparable<FAE>, Serializable
{
   /**
    * email do FAE
    */
   private String idFAE;
   /**
    * experiencia do FAE
    */
   private int experiencia;

   /**
    * Construtor vazio do FAE
    */
   public FAE()
   {

   }

   /**
    * Construtor completo do FAE
    * @param idFAE email do FAE
    * @param experiencia experiencia do FAE
    */
   public FAE(String idFAE, int experiencia)
   {
      this.idFAE = idFAE;
      this.experiencia = experiencia;
   }

   /**
    * método que permite obter ID de FAE
    * @return idFAE
    */
   public String getID()
   {
      return idFAE;
   }
   /**
    * @return descricao de FAE
    */
   @Override
   public String toString()
   {
      return "FAE{" + "idFAE=" + idFAE + ", experiencia=" + experiencia + '}';
   }

   /**
    * Reescrita do método compareTo para comparar FAE por ordem de esperiencia
    * @param outroFae objeto FAE para comparaçao da experiencia
    * @return 1, -1 ou 0 
    */
   @Override
   public int compareTo(FAE outroFae)
   {
      if (this.experiencia > outroFae.experiencia)
      {
         return 1;
      } else
      {
         if (this.experiencia == outroFae.experiencia)
         {
            return 0;
         }
         return -1;
      }
   }

}
